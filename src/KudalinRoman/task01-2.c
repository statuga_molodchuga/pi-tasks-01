#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
int main()
{
	int ft=-1, inches=-1;
	float result=.0f;
	puts("Enter a string in the following form: ft'inches");
	scanf("%d'%d", &ft, &inches);
	if (ft <0 || inches < 0)
	{
		puts("Input Error!");
		return 1;
	}
	result=(ft*12+inches)*2.54f;
	printf("A height in centimeters = %6.2f\n", result);
	return 0;
}